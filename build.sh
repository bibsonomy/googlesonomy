#!/bin/bash

APP_NAME=googlesonomy   # short-name, jar and xpi files name.
BUILT_FOR=fx         	# Application that the extension is built for, e.g. 'fx' for firefox
VERSION=0.0.1        	# Application version number

rm *.xpi
zip -vr $APP_NAME.xpi * -x@exclude.lst