// Default GoogleSonomy preferences
pref("googlesonomy.hidden", "0"); 
pref("googlesonomy.language", "");
pref("googlesonomy.username","");
pref("googlesonomy.home","http://www.bibsonomy.org/");
pref("googlesonomy.post_count","5");
pref("googlesonomy.view_mode","0");
pref("googlesonomy.list_mode","3");
pref("googlesonomy.globaltag_search_mode","0");
pref("googlesonomy.default_width","50%");
pref("googlesonomy.append_bookmark_links","1");